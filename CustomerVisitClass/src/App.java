import java.text.SimpleDateFormat;
import java.util.Date;

public class App {
    public static void main(String[] args) throws Exception {
        //4. tạo 2 customer
        Customer customer1 = new Customer("OHMM");
        Customer customer2 = new Customer("XTRA");
        System.out.println(customer1.toString());
        System.out.println(customer2.toString());

        //5.tạo 2 visit
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = format.parse("2022-07-15");
        Date date2 = format.parse("2022-06-15");
        System.out.println(date1);
        System.out.println(format.format(date1));
        Visit visit1 = new Visit(customer1, date1);
        Visit visit2 = new Visit(customer2, date2);

         System.out.println(visit1.toString());
         System.out.println(visit2.toString());

         //total expense
         System.out.println(visit1.getTotalExpense() + visit2.getTotalExpense()); 
}
}
